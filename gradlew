<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="30dp"
    android:background="#fff"
    android:layout_marginLeft="10dp"
    >

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Areas y Perimetros"
        android:textSize="30dp"
        android:textStyle="bold"
        android:textAlignment="center"
        android:layout_gravity="center"
        android:textColor="#9C27B0"/>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <ImageView
            android:layout_width="130dp"
            android:layout_height="130dp"
            android:src="@mipmap/triangu"
            android:scaleX="1"
            android:scaleY="1"
            android:id="@+id/triangulo"/>
        <ImageView
            android:layout_width="130dp"
            android:layout_height="130dp"
            android:src="@mipmap/circu_round"
            android:scaleX="1"
            android:scaleY="1"
            android:id="@+id/circulo"
            android:layout_marginLeft="20dp"/>

    </LinearLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <ImageView
            android:layout_width="130dp"
            android:layout_height="130dp"
            android:src="@mipmap/recta"
            android:scaleX="1"
            android:scaleY="1"
            android:id="@+id/rectangulo"/>
        <ImageView
            android:layout_width="130dp"
            android:layout_height="130dp"
            android:src="@mipmap/cuadra"
            android:scaleX="1"
            android:scaleY="1"
            android:id="@+id/cuadrado"
            android:layout_marginLeft="20dp"/>

    </LinearLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <ImageView
            android:layout_width="130dp"
            android:layout_height="130dp"
            android:src="@mipmap/penta"
            android:scaleX="1"
            android:scaleY="1"
            android:id="@+id/pentagono"/>
        <ImageView
            android:layout_width="130dp"
            android:layout_height="130dp"
            android:src="@mipmap/hexa"
            android:scaleX="1"
            android:scaleY="1"
            android:id="@+id/hexagono"
            android:layout_marginLeft="20dp"/>

    </LinearLayout>


</LinearLayout>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            