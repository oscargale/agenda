<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Hexagonot"
    android:orientation="vertical"
    android:padding="20dp">

    <RadioGroup
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <RadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Perimetro"
            android:id="@+id/rdbperimetroh"/>

        <RadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Area"
            android:id="@+id/rdbareah"/>

    </RadioGroup>

    <EditText
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:hint="Ingrese uno de sus lados"
        android:inputType="numberDecimal"
        android:id="@+id/lados2"/>

    <EditText
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:hint="Ingrese la apotema"
        android:inputType="numberDecimal"
        android:id="@+id/apotema2"/>



    <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Calcular"
        android:id="@+id/calcular6"/>

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:id="@+id/resultado6"/>

</LinearLayout>                                                                                                                                                                                                                                                                                                                                                                                                                                                                              